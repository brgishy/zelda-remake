﻿
namespace Zelda
{
    using UnityEngine;
    using System.Collections;

    public static class ArrayComparer
    {
        public static bool AreEqualExactly(Color[] prefabTile, Color[] tile, int width, int height)
        {
            int prefabIndex = 0;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (prefabTile[prefabIndex].a == 0) { prefabIndex++; continue; }
                    if (prefabTile[prefabIndex++] != tile[x + y * width]) return false;
                }
            }

            return true;
        }

        public static bool AreEqualMirroredX(Color[] prefabTile, Color[] tile, int width, int height)
        {
            int prefabIndex = 0;

            for (int y = 0; y < height; y++)
            {
                for (int x = width - 1; x >= 0; x--)
                {
                    if (prefabTile[prefabIndex].a == 0) { prefabIndex++; continue; }
                    if (prefabTile[prefabIndex++] != tile[x + y * width]) return false;
                }
            }

            return true;
        }

        public static bool AreEqualMirroredY(Color[] prefabTile, Color[] tile, int width, int height)
        {
            int prefabIndex = 0;

            for (int y = height - 1; y >= 0; y--)
            {
                for (int x = 0; x < width; x++)
                {
                    if (prefabTile[prefabIndex].a == 0) { prefabIndex++; continue; }
                    if (prefabTile[prefabIndex++] != tile[x + y * width]) return false;
                }
            }

            return true;
        }

        public static bool AreEqualMirroredXY(Color[] prefabTile, Color[] tile, int width, int height)
        {
            int prefabIndex = 0;

            for (int y = height - 1; y >= 0; y--)
            {
                for (int x = width - 1; x >= 0; x--)
                {
                    if (prefabTile[prefabIndex].a == 0) { prefabIndex++; continue; }
                    if (prefabTile[prefabIndex++] != tile[x + y * width]) return false;
                }
            }

            return true;
        }

        public static bool AreEqualRotation90(Color[] prefabTile, Color[] tile, int width, int height)
        {
            int prefabIndex = 0;

            for (int x = 0; x < width; x++)
            {
                for (int y = height - 1; y >= 0; y--)
                {
                    if (prefabTile[prefabIndex].a == 0) { prefabIndex++; continue; }
                    if (prefabTile[prefabIndex++] != tile[x + y * width]) return false;
                }
            }

            return true;
        }

        public static bool AreEqualRotation180(Color[] prefabTile, Color[] tile, int width, int height)
        {
            int prefabIndex = 0;

            for (int y = height - 1; y >= 0; y--)
            {
                for (int x = width - 1; x >= 0; x--)
                {
                    if (prefabTile[prefabIndex].a == 0) { prefabIndex++; continue; }
                    if (prefabTile[prefabIndex++] != tile[x + y * width]) return false;
                }
            }

            return true;
        }

        public static bool AreEqualRotation270(Color[] prefabTile, Color[] tile, int width, int height)
        {
            int prefabIndex = 0;

            for (int x = width - 1; x >= 0; x--)
            {
                for (int y = 0; y < height; y++)
                {
                    if (prefabTile[prefabIndex].a == 0) { prefabIndex++; continue; }
                    if (prefabTile[prefabIndex++] != tile[x + y * width]) return false;
                }
            }

            return true;
        }
    }
}