﻿
using System;
using System.Linq;

namespace Zelda
{
    using System.Collections.Generic;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    public class PlaceTiles
    {
        public class Tile
        {
            public string PrefabPath;
            public GameObject Prefab;
            public Color[] Image = new Color[16 * 16];
            public double ColorNumber;
        }

        public enum Orientation
        {
            Exactly,
            MirroredX,
            MirroredY,
            MirroredXY,
            Rotation90,
            Rotation180,
            Rotation270,
        }

        private const int PixelsPerUnit = 16;

        [MenuItem("CONTEXT/SpriteRenderer/Place All Tiles")]
        private static void PlaceAllTilesOnSprite(MenuCommand command)
        {
            SpriteRenderer spriteRenderer = command.context as SpriteRenderer;
            string prefabsPath = "Assets/Prefabs";
            var tilesPlacedCount = 0;

            var tiles = GetTiles(prefabsPath);
            var texture = GetTexture(spriteRenderer.sprite);

            for (int y = 0; y < texture.height; y += 16)
            {
                for (int x = 0; x < texture.width; x += 16)
                {
                    string childName = string.Format("{0:000}x{1:000}", x / 16, y / 16);

                    // already created this child, so skip it
                    if (spriteRenderer.gameObject.transform.Find(childName) != null)
                    {
                        continue;
                    }

                    Color[] tileTexture = texture.GetPixels(x, y, 16, 16);
                    double tileColorNumber = GetColorNumber(tileTexture);
                    var tileList = FindTile(tiles, tileColorNumber);
                    if (tileList == null) continue;

                    foreach (Tile tile in tileList)
                    {
                        Orientation orientation;
                        if (AreEqual(tile.Image, tileTexture, out orientation))
                        {
                            tilesPlacedCount++;
                            
                            var localX = (-texture.width / 16 / 2) + (x / 16) + 0.5f;
                            var localY = (-texture.height / 16 / 2) + (y / 16) + 0.5f;
                            var localXScale = orientation == Orientation.MirroredX || orientation == Orientation.MirroredXY ? -1 : 1;
                            var localYScale = orientation == Orientation.MirroredY || orientation == Orientation.MirroredXY ? -1 : 1;
                            
                            //TODO make sure this is a prefab instance
                            var newTile = GameObject.Instantiate(tile.Prefab) as GameObject;
                            newTile.transform.parent = spriteRenderer.transform;
                            newTile.transform.localPosition = new Vector3(localX, localY, 0);
                            newTile.transform.localScale = new Vector3(localXScale, localYScale, 1);

                            if (orientation == Orientation.Rotation90) newTile.transform.rotation = Quaternion.Euler(0, 0, -90);
                            if (orientation == Orientation.Rotation180) newTile.transform.rotation = Quaternion.Euler(0, 0, -180);
                            if (orientation == Orientation.Rotation270) newTile.transform.rotation = Quaternion.Euler(0, 0, -270);

                            newTile.name = childName;

                            break;
                        }
                    }
                }
            }

            GameObject.DestroyImmediate(texture);

            Debug.Log("Tiles Placed: " + tilesPlacedCount);
        }

        private static bool AreEqual(Color[] prefabTile, Color[] tile, out Orientation orientation)
        {
            if (ArrayComparer.AreEqualExactly(prefabTile, tile, 16, 16))
            {
                orientation = Orientation.Exactly;
                return true;
            }

            if (ArrayComparer.AreEqualMirroredX(prefabTile, tile, 16, 16))
            {
                orientation = Orientation.MirroredX;
                return true;
            }

            if (ArrayComparer.AreEqualMirroredY(prefabTile, tile, 16, 16))
            {
                orientation = Orientation.MirroredY;
                return true;
            }

            if (ArrayComparer.AreEqualMirroredXY(prefabTile, tile, 16, 16))
            {
                orientation = Orientation.MirroredXY;
                return true;
            }

            // not tested, and not sure if needed
            if (ArrayComparer.AreEqualRotation90(prefabTile, tile, 16, 16))
            {
                orientation = Orientation.Rotation90;
                return true;
            }

            if (ArrayComparer.AreEqualRotation180(prefabTile, tile, 16, 16))
            {
                orientation = Orientation.Rotation180;
                return true;
            }

            if (ArrayComparer.AreEqualRotation270(prefabTile, tile, 16, 16))
            {
                orientation = Orientation.Rotation270;
                return true;
            }

            orientation = Orientation.Exactly;
            return false;
        }

        private static List<Tile> FindTile(Dictionary<double, List<Tile>> tiles, double colorNumber)
        {
            List<Tile> tileList;
            if (tiles.TryGetValue(colorNumber, out tileList))
            {
                return tileList;
            }

            return null;
        }

        private static Texture2D GetTexture(Sprite sprite)
        {
            string path = AssetDatabase.GetAssetPath(sprite.texture);
            var textureCopy = new Texture2D(sprite.texture.width, sprite.texture.height, TextureFormat.RGBA32, false);
            textureCopy.LoadImage(File.ReadAllBytes(path));
            return textureCopy;
        }

        private static Color[] GetSpriteTexture(Sprite sprite)
        {
            Texture2D texture = GetTexture(sprite);

            Color[] result = texture.GetPixels((int)sprite.textureRect.x,
                                               (int)sprite.textureRect.y,
                                               (int)sprite.textureRect.width,
                                               (int)sprite.textureRect.height);

            GameObject.DestroyImmediate(texture);

            return result;
        }

        private static Dictionary<double, List<Tile>> GetTiles(string path)
        {
            var result = new Dictionary<double, List<Tile>>();

            var dir = new DirectoryInfo(path);

            foreach (var file in dir.GetFiles("*.prefab", SearchOption.AllDirectories))
            {
                string fullAssetPath = file.FullName.Replace("\\", "/");
                fullAssetPath = path + fullAssetPath.Substring(fullAssetPath.IndexOf(path) + path.Length);

                var prefab = AssetDatabase.LoadAssetAtPath(fullAssetPath, typeof (GameObject)) as GameObject;
                var prefabInstance = GameObject.Instantiate(prefab) as GameObject;
                var sprite = GetSprite(prefabInstance);

                if (sprite == null)
                {
                    GameObject.DestroyImmediate(prefabInstance);
                    continue;
                }

                var colors = GetSpriteTexture(sprite);

                var tile = new Tile()
                {
                    PrefabPath = fullAssetPath,
                    Prefab = prefab,
                    Image = colors,
                    ColorNumber = GetColorNumber(colors),
                };

                // if it already exists, then add to the list
                if (result.ContainsKey(tile.ColorNumber))
                {
                    result[tile.ColorNumber].Add(tile);
                }
                else
                {
                    result.Add(tile.ColorNumber, new List<Tile> { tile });
                }

                GameObject.DestroyImmediate(prefabInstance);
            }

            return result;
        }

        private static Sprite GetSprite(GameObject gameObject)
        {
            var spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
            Array.Sort(spriteRenderers, SpriteRendererComparison);
            Sprite sprite = spriteRenderers[0].sprite;
            Rect rect = sprite.textureRect;
            return rect.width == 16f && rect.height == 16f ? sprite : null;
        }

        private static int SpriteRendererComparison(SpriteRenderer s1, SpriteRenderer s2)
        {
            //TODO if equal again, maybe check z?
            int result = s2.sortingLayerID.CompareTo(s1.sortingLayerID);
            return result == 0 ? s2.sortingOrder.CompareTo(s1.sortingOrder) : result;
        }

        private static double GetColorNumber(Color[] colors)
        {
            double sum = 0;

            for (int i = 0; i < colors.Length; i++)
            {
                sum += (double)colors[i].r + (double)colors[i].g + (double)colors[i].b;
            }

            return ((int)(sum * 1000)) / 1000.0;
        }
    }
}