﻿
using System;
using System.Linq;

namespace Zelda
{
    using System.Collections.Generic;
    using System.IO;
    using UnityEditor;
    using UnityEngine;

    public class PlaceNon16x16Tiles
    {
        public class Tile
        {
            public string PrefabPath;
            public GameObject Prefab;
            public Color[] Image;
            public int Width;
            public int Height;
        }

        public enum Orientation
        {
            Exactly,
            MirroredX,
        }

        [MenuItem("CONTEXT/SpriteRenderer/Place All Non 16x16 Tiles")]
        private static void PlaceAllNon16x16TilesOnSprite(MenuCommand command)
        {
            SpriteRenderer spriteRenderer = command.context as SpriteRenderer;
            var texture = GetTexture(spriteRenderer.sprite);
            var tiles = GetTiles("Assets/Prefabs"); // /MainWorld/Statues");
            var tilesPlacedCount = 0;

            for (int y = 0; y < texture.height; y += 16)
            {
                for (int x = 0; x < texture.width; x += 16)
                {
                    string childName = string.Format("{0:000}x{1:000}", x / 16, y / 16);

                    // already created this child, so skip it
                    if (spriteRenderer.gameObject.transform.Find(childName) != null) continue;

                    foreach (Tile tile in tiles)
                    {
                        if (x + tile.Width > texture.width || y + tile.Height > texture.height)
                            continue;

                        Color[] tileTexture = texture.GetPixels(x, y, tile.Width, tile.Height);

                        Orientation orientation;
                        if (AreEqual(tile.Image, tileTexture, tile.Width, tile.Height, out orientation))
                        {
                            tilesPlacedCount++;

                            var localX = (-texture.width / 16 / 2) + (x / 16) + (tile.Width / 16.0f * 0.5f);
                            var localY = (-texture.height / 16 / 2) + (y / 16) + (tile.Height / 16.0f * 0.5f);
                            var localXScale = orientation == Orientation.MirroredX ? -1 : 1;

                            //TODO make sure this is a prefab instance
                            var newTile = GameObject.Instantiate(tile.Prefab) as GameObject;
                            newTile.transform.parent = spriteRenderer.transform;
                            newTile.transform.localPosition = new Vector3(localX, localY, 0);
                            newTile.transform.localScale = new Vector3(localXScale, 1, 1);
                            newTile.name = childName;

                            break;
                        }
                    }
                }
            }

            GameObject.DestroyImmediate(texture);

            Debug.Log("Tiles Placed: " + tilesPlacedCount);
        }

        private static bool AreEqual(Color[] prefabTile, Color[] tile, int width, int height, out Orientation orientation)
        {
            if (ArrayComparer.AreEqualExactly(prefabTile, tile, width, height))
            {
                orientation = Orientation.Exactly;
                return true;
            }

            if (ArrayComparer.AreEqualMirroredX(prefabTile, tile, width, height))
            {
                orientation = Orientation.MirroredX;
                return true;
            }

            orientation = Orientation.Exactly;
            return false;
        }

        private static Texture2D GetTexture(Sprite sprite)
        {
            string path = AssetDatabase.GetAssetPath(sprite.texture);
            var textureCopy = new Texture2D(sprite.texture.width, sprite.texture.height, TextureFormat.RGBA32, false);
            textureCopy.LoadImage(File.ReadAllBytes(path));
            return textureCopy;
        }

        private static Color[] GetSpriteTexture(Sprite sprite)
        {
            Texture2D texture = GetTexture(sprite);

            Color[] result = texture.GetPixels((int)sprite.textureRect.x,
                                               (int)sprite.textureRect.y,
                                               (int)sprite.textureRect.width,
                                               (int)sprite.textureRect.height);

            GameObject.DestroyImmediate(texture);

            return result;
        }

        private static List<Tile> GetTiles(string path)
        {
            var result = new List<Tile>();
            var dir = new DirectoryInfo(path);

            foreach (var file in dir.GetFiles("*.prefab", SearchOption.AllDirectories))
            {
                string fullAssetPath = file.FullName.Replace("\\", "/");
                fullAssetPath = path + fullAssetPath.Substring(fullAssetPath.IndexOf(path) + path.Length);

                var prefab = AssetDatabase.LoadAssetAtPath(fullAssetPath, typeof(GameObject)) as GameObject;
                var prefabInstance = GameObject.Instantiate(prefab) as GameObject;
                var sprite = GetSprite(prefabInstance);
                int width = (int)sprite.textureRect.width;
                int height = (int)sprite.textureRect.height;

                if (MultiPartSprite(prefabInstance))
                {
                    GameObject.DestroyImmediate(prefabInstance);
                    continue;
                }

                if (sprite == null || (width == 16 && height == 16))
                {
                    GameObject.DestroyImmediate(prefabInstance);
                    continue;
                }

                var tile = new Tile()
                {
                    PrefabPath = fullAssetPath,
                    Prefab = prefab,
                    Image = GetSpriteTexture(sprite),
                    Width = width,
                    Height = height,
                };

                result.Add(tile);

                GameObject.DestroyImmediate(prefabInstance);
            }

            return result;
        }

        private static bool MultiPartSprite(GameObject gameObject)
        {
            var spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
            
            Vector3 center = spriteRenderers[0].sprite.bounds.center;
            Vector3 extents = Vector3.Scale(spriteRenderers[0].sprite.bounds.extents, spriteRenderers[0].transform.localScale);

            foreach (var spriteRenderer in spriteRenderers)
            {
                Vector3 spriteExtents = Vector3.Scale(spriteRenderer.sprite.bounds.extents, spriteRenderer.transform.localScale);
                if (spriteRenderer.sprite.bounds.center != center || spriteExtents != extents)
                    return true;
            }

            return false;
        }

        private static Sprite GetSprite(GameObject gameObject)
        {
            var spriteRenderers = gameObject.GetComponentsInChildren<SpriteRenderer>();
            Array.Sort(spriteRenderers, SpriteRendererComparison);
            return spriteRenderers[0].sprite;
        }

        private static int SpriteRendererComparison(SpriteRenderer s1, SpriteRenderer s2)
        {
            //TODO if equal again, maybe check z?
            int result = s2.sortingLayerID.CompareTo(s1.sortingLayerID);
            return result == 0 ? s2.sortingOrder.CompareTo(s1.sortingOrder) : result;
        }
    }
}