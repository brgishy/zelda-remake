﻿

namespace Zelda
{
    using System;
    using UnityEditor;
    using UnityEngine;
    
    public static class TransformConextMenuItems
    {
        [MenuItem("CONTEXT/Transform/Align To Nearest 16")]
        private static void AlignToNearest16(MenuCommand command)
        {
            Transform t = command.context as Transform;

            t.localPosition = new Vector2((int)Math.Round((t.localPosition.x / 16.0), MidpointRounding.AwayFromZero) * 16f,
                                          (int)Math.Round((t.localPosition.y / 16.0), MidpointRounding.AwayFromZero) * 16f);
        }

        [MenuItem("CONTEXT/Transform/Kill All Children")]
        private static void KillAllChildren(MenuCommand command)
        {
            Transform t = command.context as Transform;

            while (t.childCount > 0)
            {
                GameObject.DestroyImmediate(t.GetChild(0).gameObject);
            }
        }
    }
}